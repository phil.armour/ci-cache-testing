FROM ubuntu:18.04

RUN apt update 
RUN apt install -y postgresql-client

ENTRYPOINT [ "psql", "-h", "localhost", "-p", "5432", "-U", "root", "root" ]